import csv


def read_file(path: str, delimiter=","):
    res: dict[str, list[str]] = {}
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=delimiter)
        header = next(csv_reader)
        for h in header:
            res[h] = []
        for row in csv_reader:
            for idx, h in enumerate(header):
                res[h].append(row[idx])
    return res


def count(quantities: dict[str, list[str]], sent: dict[str, list[str]]):
    quantites = list(zip(quantities["Item number"], quantities["Nb pdt / carton"]))
    envoyes = list(
        zip(sent["CodeArticle"], sent["QuantiteCommande"], sent["NumeroCommande"])
    )

    acc = []
    for envoye in envoyes:
        for quantite in quantites:
            if envoye[0] == quantite[0]:  # meme article
                if envoye[1] == quantite[1]:  # meme quantité
                    acc.append(envoye)
                    break
                if int(envoye[1]) % int(quantite[1]) == 0:  # multiple du carton
                    acc.append(envoye)
                    break

    return acc


if __name__ == "__main__":
    quantites = read_file("quantites.csv")
    envoyes = read_file("envoyes.csv", ";")

    data = count(quantites, envoyes)

    with open("results.csv", "w") as f:
        writer = csv.writer(f)
        writer.writerow(["Code", "Quantite", "NumeroCommande"])
        writer.writerows(data)
